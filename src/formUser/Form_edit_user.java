package formUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.Usuario;


public class Form_edit_user extends JFrame{
	
	private JLabel			lblID 		= new JLabel("ID:");
	private JTextField		txtID 		= new JTextField();
	private JLabel			lblUsuario 	= new JLabel("Usu�rio:");
	private JTextField 		txtEmail 	= new JTextField();
	private JLabel			lblEmail 	= new JLabel("Email:");
	private JTextField 		txtUsuario 	= new JTextField();
	private JLabel			lblSenha 	= new JLabel("Senha:");
	private JPasswordField 	txtSenha 	= new JPasswordField();
	private JLabel			lblNivel 	= new JLabel("Nivel:");
	private JTextField 		txtNivel 	= new JTextField();
	private JButton			btnEnviar 	= new JButton("Editar");
	private JButton			btnVoltar 	= new JButton("Voltar");
	private JLabel			lblMsg 		= new JLabel("_____________________________");
	
	public Form_edit_user() {
		this.setTitle("Editar Usu�rio");
		this.setBounds(250, 250, 300, 300);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lblID.setBounds		(  5,   5, 100, 30);
		txtID.setBounds		(110,   5, 150, 30);
		lblUsuario.setBounds(  5,  40, 100, 30);
		txtUsuario.setBounds(110,  40, 150, 30);;
		lblEmail.setBounds	(  5,  75, 100, 30);
		txtEmail.setBounds	(110,  75, 150, 30);
		lblSenha.setBounds	(  5, 110, 100, 30);
		txtSenha.setBounds	(110, 110, 150, 30);
		lblNivel.setBounds	(  5, 145, 100, 30);
		txtNivel.setBounds	(110, 145, 150, 30);
		btnEnviar.setBounds	(140, 180, 120, 30);
		btnVoltar.setBounds	(  5, 180, 120, 30);
		lblMsg.setBounds	(  5, 215, 300, 30);
		
		this.add(lblID);
		this.add(txtID);
		this.add(lblUsuario);
		this.add(txtUsuario);
		this.add(lblEmail);
		this.add(txtEmail);
		this.add(lblSenha);
		this.add(txtSenha);
		this.add(lblNivel);
		this.add(txtNivel);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario(txtID.getText(), txtUsuario.getText(), txtEmail.getText(), txtSenha.getText(), txtNivel.getText());
				if (usuario.save() == true){
					new Users_list();
					dispose();
				}else{
					lblMsg.setText("Falha na Edi��o");
				}
				
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Users_list();
				dispose();
			}
		});
		
		this.setVisible(true);
	}

}
