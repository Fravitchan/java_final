package formLivro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.Livro;

public class Livros_list extends JFrame{

	private JTable tabela;
	private JScrollPane sPnl;
	private JPanel painelFundo;
	private JButton btnAdd;
	private JButton btnEdita;
	private JButton btnDelete;
	private DefaultTableModel modelo = new DefaultTableModel();
	
	public void setDados(DefaultTableModel modelo){
			
		Livro livro = new Livro("", "", "", "", "");
		ResultSet rs = livro.listall();
		this.modelo.setNumRows(0);

		try {
			while(rs.next())
			{
				this.modelo.addRow(new Object[]{rs.getInt("id_livro"), ""+rs.getString("titulo"), ""+rs.getString("autor"), ""+rs.getString("cpf_cliente"), ""+rs.getString("alugado")});
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Livros_list(){
		
		this.setBounds(250, 250, 500, 300);
		
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Biblioteca");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(600, 400);
		
		this.tabela = new JTable(this.modelo);
		this.modelo.addColumn("ID");
		this.modelo.addColumn("Titulo");
		this.modelo.addColumn("Autor");
		this.modelo.addColumn("CPF do cliente");
		this.modelo.addColumn("Status");
		this.tabela.getColumnModel().getColumn(0).setPreferredWidth(10);
		tabela.getColumnModel().getColumn(1).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        
        this.setDados(modelo);
        
		this.sPnl = new JScrollPane(this.tabela);
		this.painelFundo.add(sPnl);
		
		this.btnAdd = new JButton("Cadastrar");
		this.btnAdd.setBounds(360, 45, 150, 30);
		this.btnEdita = new JButton("Editar");
		this.btnEdita.setBounds(360, 90, 150, 30);
		this.btnDelete = new JButton("Excluir");
		this.btnDelete.setBounds(360, 135, 150, 30);
		
		this.add(this.btnAdd);
		this.add(this.btnEdita);
		this.add(this.btnDelete);
		
		this.getContentPane().add(painelFundo);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_cad_livro();
				dispose();
			}
		});
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_edit_livro();
				dispose();
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_delete_livro();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Livros_list();
	}
	
}
