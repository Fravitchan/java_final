package formLivro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.Livro;

public class Form_cad_livro extends JFrame{
		
		private JLabel			lblLivro 	= new JLabel("T�tulo:");
		private JTextField 		txtLivro 	= new JTextField();
		private JLabel			lblAutor 	= new JLabel("Autor:");
		private JTextField 		txtAutor 	= new JTextField();
		private JLabel			lblCpf_cliente 	= new JLabel("CPF do cliente:");
		private JPasswordField 	txtCpf_cliente 	= new JPasswordField();
		private JLabel			lblStatus 	= new JLabel("Status:");
		private JTextField 		txtStatus 	= new JTextField();
		private JButton			btnEnviar 	= new JButton("Cadastrar");
		private JButton			btnVoltar 	= new JButton("Voltar");
		private JLabel			lblMsg 		= new JLabel("_______________________________________");
		
		public Form_cad_livro() {
			this.setTitle("Cadastro do Livro");
			this.setBounds(250, 250, 300, 250);
			this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			this.setLayout(null);
			
			lblLivro.setBounds(  5,   5, 100, 30);
			txtLivro.setBounds(110,   5, 150, 30);
			lblAutor.setBounds	(  5,  40, 100, 30);
			txtAutor.setBounds	(110,  40, 150, 30);
			lblCpf_cliente.setBounds	(  5,  75, 100, 30);
			txtCpf_cliente.setBounds	(110,  75, 150, 30);
			lblStatus.setBounds	(  5, 110, 100, 30);
			txtStatus.setBounds	(110, 110, 150, 30);
			btnEnviar.setBounds	(140, 150, 120, 30);
			btnVoltar.setBounds	(  5, 150, 120, 30);
			lblMsg.setBounds	(  5, 200, 300, 30);
			
			this.add(lblLivro);
			this.add(txtLivro);
			this.add(lblAutor);
			this.add(txtAutor);
			this.add(lblCpf_cliente);
			this.add(txtCpf_cliente);
			this.add(lblStatus);
			this.add(txtStatus);
			this.add(btnEnviar);
			this.add(btnVoltar);
			this.add(lblMsg);
			
			
			btnEnviar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Livro livro = new Livro("", txtLivro.getText(), txtAutor.getText(), txtCpf_cliente.getText(), txtStatus.getText());
					if (livro.save() == true){
						new Livros_list();
						dispose();
					}else{
						lblMsg.setText("Falha ao realizar o cadastro.");
					}
				}
			});
			
			btnVoltar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new Livros_list();
					dispose();
				}
			});
			
			this.setVisible(true);
		}
		
		public static void main(String[] args) {
			new Form_cad_livro();
		}
		

}
