package formUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.Usuario;


public class Users_list extends JFrame{
	
	private JTable tabela;
	private JScrollPane sPnl;
	private JPanel painelFundo;
	private JButton btnInseri;
	private JButton btnEdita;
	private JButton btnExclui;
	
	public Users_list()
	{
		this.setBounds(250, 250, 500, 300);
		
		
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Usu�rios");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
        
		
		this.btnInseri = new JButton("Cadastrar");
		this.btnInseri.setBounds(50, 45, 150, 30);
		this.btnEdita = new JButton("Editar");
		this.btnEdita.setBounds(50, 90, 150, 30);
		this.btnExclui = new JButton("Excluir");
		this.btnExclui.setBounds(50, 135, 150, 30);
		
		this.add(this.btnInseri);
		this.add(this.btnEdita);
		this.add(this.btnExclui);
		
		this.getContentPane().add(painelFundo);
		
		btnInseri.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_cad_user();
				dispose();
			}
		});
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_edit_user();
				dispose();
			}
		});
		
		btnExclui.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_delete_user();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Users_list();
	}

}
