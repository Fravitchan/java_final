package formLivro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.Livro;

public class Form_edit_livro extends JFrame{
	
	private JLabel			lblID 		= new JLabel("ID:");
	private JTextField		txtID 		= new JTextField();
	private JLabel			lblLivro 	= new JLabel("Livro:");
	private JTextField 		txtLivro 	= new JTextField();
	private JTextField 		txtTitulo 	= new JTextField();
	private JLabel			lblTitulo 	= new JLabel("Titulo:");
	private JLabel			lblAutor 	= new JLabel("Autor:");
	private JPasswordField 	txtAutor 	= new JPasswordField();
	private JLabel			lblStatus 	= new JLabel("Status:");
	private JTextField 		txtStatus 	= new JTextField();
	private JButton			btnEnviar 	= new JButton("Editar");
	private JButton			btnVoltar 	= new JButton("Voltar");
	private JLabel			lblMsg 		= new JLabel("_______________________________________");
	
	public Form_edit_livro() {
		this.setTitle("Editar Livro");
		this.setBounds(250, 250, 300, 300);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lblID.setBounds		(  5,   5, 100, 30);
		txtID.setBounds		(110,   5, 150, 30);
		lblLivro.setBounds(  5,  40, 100, 30);
		txtLivro.setBounds(110,  40, 150, 30);;
		lblTitulo.setBounds	(  5,  75, 100, 30);
		txtTitulo.setBounds	(110,  75, 150, 30);
		lblAutor.setBounds	(  5, 110, 100, 30);
		txtAutor.setBounds	(110, 110, 150, 30);
		lblStatus.setBounds	(  5, 145, 100, 30);
		txtStatus.setBounds	(110, 145, 150, 30);
		btnEnviar.setBounds	(140, 180, 120, 30);
		btnVoltar.setBounds	(  5, 180, 120, 30);
		lblMsg.setBounds	(  5, 215, 300, 30);
		
		this.add(lblID);
		this.add(txtID);
		this.add(lblLivro);
		this.add(txtLivro);
		this.add(lblTitulo);
		this.add(txtTitulo);
		this.add(lblAutor);
		this.add(txtAutor);
		this.add(lblStatus);
		this.add(txtStatus);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("deprecation")
				Livro livro = new Livro(txtID.getText(), txtLivro.getText(), txtTitulo.getText(), txtAutor.getText(), txtStatus.getText());
				if (livro.save() == true){
					new Livros_list();
					dispose();
				}else{
					lblMsg.setText("Erro: n�o foi poss�vel editar.");
				}
				
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Livros_list();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
}

