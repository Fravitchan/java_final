package formUser;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import database.Livro;
import formLivro.Livros_list;

import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Adm extends JFrame{

	private JTable tabela;
	private JScrollPane sPnl;
	private JPanel painelFundo;
	private JButton btnUsuario;
	private JButton btnLivro;
	
	public Adm(){
		
		this.setBounds(250, 250, 500, 300);
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Painel Administrativo");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(450, 300);
		
		this.btnUsuario = new JButton("Usuario");
		this.btnUsuario.setBounds(150, 50, 150, 30);
		this.btnLivro = new JButton("Livro");
		this.btnLivro.setBounds(150, 90, 150, 30);
		
		this.add(this.btnUsuario);
		this.add(this.btnLivro);
		
		this.getContentPane().add(painelFundo);
		
		btnUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Users_list();
				dispose();
			}
		});
		
		btnLivro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Livros_list();
				dispose();
			}
		});
			
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Adm();
	}
	
}
