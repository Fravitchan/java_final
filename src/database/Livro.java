package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Livro {
	
	private String id_livro;
	private String titulo; 
	private String autor;	 
	private String cpf_cliente; 
	private String alugado;
	
	private String tableName	= "biblioteca.livros";
	private String fieldsName 	= "titulo, autor, id_livro, cpf_cliente, alugado";
	private String fieldKey		= "id";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	
	public Livro(String id_livro, String titulo, String autor, String cpf_cliente, String alugado) {
		this.id_livro     = id_livro;
		this.titulo       = titulo;
		this.autor        = autor;
		this.cpf_cliente  = cpf_cliente;
		this.alugado      = alugado;
	}
	
	private String[] toArray() {
		return(
			new String[]{
				this.id_livro,
				this.titulo, 
				this.autor,
				this.cpf_cliente,
				this.alugado				
			}
		);

	}
	
	public boolean save() {
		if (this.titulo == ""){
			this.titulo = "0";
			return dbQuery.insert(this.toArray());
		}else{
			return dbQuery.update(this.toArray());
		}
	}
	
	public boolean delete() {
		if (this.titulo != ""){
			return dbQuery.delete(this.toArray());
		}
		return false;
	}
	
	/*public boolean checkLogin() {
		
		ResultSet rs =	dbQuery.select(" (autor = '"+this.autor+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
	 	try {
	 		return( rs.next() );
		 /*
			if ( rs.next() ){
			 return( true );
		 	}	else {
			 	return( false );
		 	}
		 
	 	} catch (SQLException e) {
			e.printStackTrace();
		}
	 	return( false );
	}*/
	
	public ResultSet listall() {
		return(dbQuery.select(""));
	}
	
	public String getId_livro() {
		return id_livro;
	}
	public void setId_livro(String id_livro) {
		this.id_livro = id_livro;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getCpf_cliente() {
		return cpf_cliente;
	}
	public void setCpf_cliente(String cpf_cliente) {
		this.cpf_cliente = cpf_cliente;
	}
	public String getAlugado() {
		return alugado;
	}
	public void setAlugado(String alugado) {
		this.alugado = alugado;
	}

	
}
