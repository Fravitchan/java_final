package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario {
	
	private String idUser; 
	private String user_name;
	private String email; 
	private String senha; 
	private String nivel;
	
	private String tableName	= "biblioteca.usuarios";
	private String fieldsName 	= "idUsers, user_name, email, senha, nivel";
	private String fieldKey		= "idUsers";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	
	public Usuario(String idUser, String user_name, String email, String  senha, String nivel) {
		this.idUser    = idUser;
		this.user_name = user_name;
		this.email     = email;
		this.senha     = senha;
		this.nivel     = nivel;
	}
	
	private String[] toArray() {
		return(
			new String[]{
				this.idUser, 
				this.user_name,
				this.email,
				this.senha,
				this.nivel				
			}
		);

	}
	
	public boolean save() {
		if (this.idUser == ""){
			this.idUser = "0";
			return dbQuery.insert(this.toArray());
		}else{
			return dbQuery.update(this.toArray());
		}
	}
	
	public boolean delete() {
		if (this.idUser != ""){
			return dbQuery.delete(this.toArray());
		}
		return false;
	}
	
	public boolean checkLogin() {
		
		ResultSet rs =	dbQuery.select(" (user_name = '"+this.user_name+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
	 	try {
	 		return( rs.next() );
		 /*
			if ( rs.next() ){
			 return( true );
		 	}	else {
			 	return( false );
		 	}
		 */
	 	} catch (SQLException e) {
			e.printStackTrace();
		}
	 	return( false );
	}
	
	public ResultSet listall() {
		return(dbQuery.select(""));
	}
	
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

}