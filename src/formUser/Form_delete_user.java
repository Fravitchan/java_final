package formUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;



import database.Usuario;

public class Form_delete_user {

	private JLabel			lblId 	= new JLabel("ID:");
	private JTextField 		txtId 	= new JTextField();
	private JButton			btnEnviar 	= new JButton("Excluir");
	private JButton			btnVoltar 	= new JButton("Voltar");
	private JLabel			lblMsg 		= new JLabel("_______________________________________");
	
	public Form_delete_user(){
		this.setTitle("Excluir Usu�rio");
		this.setBounds(250, 250, 300, 150);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lblId.setBounds		(  5,  5, 100, 30);
		txtId.setBounds		(110,  5, 150, 30);
		btnEnviar.setBounds	(140, 40, 120, 30);
		btnVoltar.setBounds	(  5, 40, 120, 30);
		lblMsg.setBounds	(  5, 75, 300, 30);
		
		this.add(lblId);
		this.add(txtId);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario(txtId.getText(), "", "", "", "");
				if (usuario.delete() == true){
					new Users_list();
					dispose();
				}else{
					lblMsg.setText("Falha ao Excluir");
				}
				
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Users_list();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Form_delete_user();
	}
	
}
