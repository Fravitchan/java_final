package formUser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import database.Usuario;

public class Form_cad_user extends JFRame{

	private JLabel			lblUsuario 	= new JLabel("Usu�rio:");
	private JTextField 		txtEmail 	= new JTextField();
	private JLabel			lblEmail 	= new JLabel("Email:");
	private JTextField 		txtUsuario 	= new JTextField();
	private JLabel			lblSenha 	= new JLabel("Senha:");
	private JPasswordField 	txtSenha 	= new JPasswordField();
	private JLabel			lblNivel 	= new JLabel("Nivel:");
	private JTextField 		txtNivel 	= new JTextField();
	private JButton			btnEnviar 	= new JButton("Cadastrar");
	private JButton			btnVoltar 	= new JButton("Voltar");
	private JLabel			lblMsg 		= new JLabel("__________________________");
	
	public Form_cad_user() {
		this.setTitle("Cadastro do Usu�rio");
		this.setBounds(250, 250, 300, 250);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lblUsuario.setBounds(  5,   5, 100, 30);
		txtUsuario.setBounds(110,   5, 150, 30);
		lblEmail.setBounds	(  5,  40, 100, 30);
		txtEmail.setBounds	(110,  40, 150, 30);
		lblSenha.setBounds	(  5,  75, 100, 30);
		txtSenha.setBounds	(110,  75, 150, 30);
		lblNivel.setBounds	(  5, 110, 100, 30);
		txtNivel.setBounds	(110, 110, 150, 30);
		btnEnviar.setBounds	(140, 150, 120, 30);
		btnVoltar.setBounds	(  5, 150, 120, 30);
		lblMsg.setBounds	(  5, 200, 300, 30);
		
		this.add(lblUsuario);
		this.add(txtUsuario);
		this.add(lblEmail);
		this.add(txtEmail);
		this.add(lblSenha);
		this.add(txtSenha);
		this.add(lblNivel);
		this.add(txtNivel);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario("", txtUsuario.getText(), txtEmail.getText(), txtSenha.getText(), txtNivel.getText());
				if (usuario.save() == true){
					new Users_list();
					dispose();
				}else{
					lblMsg.setText("Falha do Cadastro");
				}
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Users_list();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Form_cad_user();
	}
	
	
}
